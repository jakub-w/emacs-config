(global-set-key (kbd "M-o") #'other-window)
(global-set-key (kbd "C-z") #'help-command)
(global-set-key (kbd "C-h") #'backward-delete-char)
(global-set-key (kbd "M-h") #'backward-kill-word)
(global-set-key (kbd "C-x C-b") #'switch-to-buffer)

(if (> emacs-major-version 26)
    (progn (fido-mode 1)
	   (when (> emacs-major-version 27)
	     (fido-vertical-mode 1)))
  (ido-mode 1)
  (icomplete-mode 1))

(menu-bar-mode -1)

(electric-pair-mode 1)
(show-paren-mode 1)
(global-so-long-mode 1)

(setopt inhibit-startup-screen t)
