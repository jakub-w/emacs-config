;; -*- lexical-binding: t -*-

(require 'subr-x)
(require 'lyrics)

;; TODO: save lyrics to a song file with mid3v2 (check ~/Notes/mid3v2.org)
(defun my-lyrics ()
  "Gets lyrics for a song playing in MOC player. Requires lyrics package."
  (interactive)
  ;; check if mocp is running
  (let ((info (split-string (shell-command-to-string
			     "mocp -Q '%file\t%artist\t%song'")
			    "\t" nil "\n")))
    (cond
     ((not (file-exists-p (substitute-env-in-file-name "$HOME/.moc/pid")))
      (message "MOC Player is not running."))
     ((string-empty-p (car info))
      (message "MOC is not playing anything."))
     ((or (string-empty-p (cadr info))
	  (string-empty-p (caddr info)))
      (message "Could not determine song title or artist.\nPlease check if metadata is present in current playing file (ID3 tags for mp3, Vorbis comments for flac, etc.)"))
     (t (lyrics (cadr info) (caddr info))))))

;; This could be used to set the default behavior
;; (setq lyrics-current-song-function ...)

;; key for refreshing lyrics if there's playing a new song
(define-key lyrics-show-mode-map (kbd "g") #'my-lyrics)
(define-key lyrics-show-mode-map (kbd "n") #'scroll-up-line)
(define-key lyrics-show-mode-map (kbd "<down>") #'scroll-up-line)
(define-key lyrics-show-mode-map (kbd "p") #'scroll-down-line)
(define-key lyrics-show-mode-map (kbd "<up>") #'scroll-down-line)
(define-key lyrics-show-mode-map (kbd "G")
  #'(lambda () (interactive)
      (setq current-prefix-arg "t")
      (call-interactively 'lyrics)))


(defun lyrics--transliterate-to-ascii (str)
  (shell-command-to-string (format "echo -n %s | iconv -t ASCII//TRANSLIT"
				   (shell-quote-argument str))))

(defun lyrics--downgrade-encoding-advice (oldfun artist song)
  (funcall oldfun
	 (lyrics--transliterate-to-ascii artist)
	 (lyrics--transliterate-to-ascii song)))

(advice-add 'lyrics-azlyrics-url :around
	    #'lyrics--downgrade-encoding-advice)

;;; New backend: lyrics.camp

(defun lyrics-lyrics-camp-url (artist song)
  "Return an lyrics.camp url for ARTIST SONG."
  (cl-labels ((cleanup (string)
                       (downcase
			(replace-regexp-in-string
			 "-\\_>" ""
			 (replace-regexp-in-string
			  "[[:space:][:punct:]]+" "-"
			  (lyrics--transliterate-to-ascii string))))))
    (format "https://www.lyrics.camp/%s/%s.html" (cleanup artist) (cleanup song))))

(defun lyrics-lyrics-camp-page-callback (status artist song &optional buffer)
  (if-let (err (plist-get status :error))
      (if (= url-http-response-status 404)
          (signal 'lyrics-not-found (list artist song))
        (message (error-message-string err)))
    (let* ((dom (lyrics-url-retrieve-parse-html (current-buffer)))
	   (node (assoc 'p (car (dom-by-class dom "txly1"))))
           (lyrics (string-trim
		    (replace-regexp-in-string
		     "[[:blank:]]+\n" "\n"
		     (replace-regexp-in-string
		      "\n ?\n" "\n"
		      (replace-regexp-in-string
		       "" ""
		       (lyrics-node-texts node)))))))
      (when (string-empty-p lyrics)
	(signal 'lyrics-not-found (list artist song)))
      (lyrics-show artist song lyrics buffer 'save))))


(defun lyrics-lyrics-camp (artist song &optional buffer)
  (url-retrieve (lyrics-lyrics-camp-url artist song)
		#'lyrics-lyrics-camp-page-callback
		(list artist song buffer)))

(setq lyrics-backend #'lyrics-lyrics-camp)
