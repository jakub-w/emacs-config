;; -*- lexical-binding: t -*-

(require 'elfeed)
(require 'subr-x)
(require 'cl-lib)

(let* ((sentinel
	(lambda (process event)
	  (unless (process-live-p process)
	    (with-current-buffer (process-buffer process)
	      (if (= (process-exit-status process) 0)
		  (kill-buffer (current-buffer))
		(ansi-color-apply-on-region (point-min) (point-max))
		(replace-regexp-in-region "" "" (point-min) (point-max))
		(special-mode)
		(pop-to-buffer (current-buffer))))))))
  (defun my-start-process-show-errors (name program &rest program-args)
    (let* ((buffer (generate-new-buffer (format " *%s*" name))))
      (make-process :name name
		    :buffer buffer
		    :command (cons program program-args)
		    :sentinel sentinel))))

(defvar my-elfeed-mpv-last-played nil)

(defun my-elfeed-mpv-play-last ()
  (interactive)
  (if my-elfeed-mpv-last-played
      (my-elfeed-mpv-play my-elfeed-mpv-last-played)
    (user-error "Nothing played before")))

(defun my-elfeed-mpv-play (url &optional no-video)
  "Play URL in MPV player.

NO-VIDEO should be set to non-nil to start MPV with --no-video option in
a terminal application.

When used interactively URL is the url at point.
NO-VIDEO can be set with a prefix argument \\[universal-argument]."
  (interactive (list (or (thing-at-point 'url) (read-string "URL: "))
		     current-prefix-arg))
  (cl-check-type url string "Expected URL as string")
  (setq my-elfeed-mpv-last-played url)
  (cond (no-video
	 (my-start-process-show-errors
	  "mpv"
	  "alacritty"
	  "-e" ;(concat "mpv --no-video '" url "'")
	  "mpv" "--no-video" "--force-window=no" "--save-position-on-quit"
	  url))
	((string-match-p "https?://\\(?:www\\.\\)?bitchute\\." url)
	 (my-start-process-show-errors
	  "mpv" "mpv"
	  ;; (format "--background-color=%s"
	  ;; 	  (face-attribute 'default :background))
	  url))
	(t
	 (my-start-process-show-errors
	  "mpv" "mpv"
	  ;; (format "--background-color=%s"
	  ;; 	  (face-attribute 'default :background))
	  (let ((max-height (or (display-pixel-height) 1080)))
	    (concat "--ytdl-format="
		    (format (string-join '("best[height=%s]"
					   "bestvideo[height=%s]+bestaudio"
					   "bestvideo[height<=%s]+bestaudio"
					   "best[height<=%s]"
					   "best")
					 "/")
			    max-height max-height max-height max-height)))
	  ;; Workaround for youtube-dl not working properly with youtube
	  ;; as of 12.09.2021.
	  (concat "--script-opts=ytdl_hook-ytdl_path="
		  (or (executable-find "yt-dlp")
		      (concat (getenv "HOME")
			      "/Programming/yt-dlp/yt-dlp")))
	  "--save-position-on-quit"
	  url)))
  (message "Playing from '%s'..." url))

(defun my-elfeed--entry-play (entry)
  (let* ((enclosures (elfeed-entry-enclosures entry)))

    (cond
     ;; If there's a video in enclosures, open that, don't look at the link.
     ((and enclosures
	   (string-match-p "^video/"
			   (nth 1 (car enclosures))))
      (my-elfeed-mpv-play (caar enclosures)))
     ;; If there is an audio file in enclosures, open it
     ;; with --no-video
     ((and enclosures
	   (string-match-p "^audio/"
			   (nth 1 (car enclosures))))
      (my-elfeed-mpv-play (caar enclosures) 'no-video))
      ;; else open the link in mpv proper
     (t (my-elfeed-mpv-play (elfeed-entry-link entry))))))

(defun my-elfeed-open-link ()
  (interactive)
  (unless (member major-mode '(elfeed-search-mode elfeed-show-mode))
    (user-error "[Error] Wrong mode to do that"))
  (if (eq major-mode 'elfeed-search-mode)
      (let ((entries (elfeed-search-selected)))
	(cl-loop
	 for entry in entries
	 do (elfeed-untag entry 'unread)
	 when (elfeed-entry-link entry)
	 do (progn
	      (my-elfeed--entry-play entry)
	      (elfeed-search-show-entry entry)))
	(mapc #'elfeed-search-update-entry entries))
    (my-elfeed--entry-play elfeed-show-entry)))

(define-key elfeed-search-mode-map (kbd "v") #'my-elfeed-open-link)
(define-key elfeed-show-mode-map (kbd "v") #'my-elfeed-open-link)
(global-set-key (kbd "C-c v") #'my-elfeed-mpv-play-last)

(defun my-elfeed-update-feed-local (file)
  "Like `elfeed-update-feed' but uses local FILE, not url.

Designed to use as advice to `elfeed-update-feed' with `:before-until' combinator."
  (if (string-prefix-p "file:" file)
      (let ((file-path (string-trim-left file "file:")))
	(if (not (file-exists-p file-path))
	    (elfeed-handle-parse-error file "File doesn't exist.")
	  (progn
	    (unless elfeed--inhibit-update-init-hooks
	      (run-hooks 'elfeed-update-hooks))
	    (condition-case error
		(let ((feed (elfeed-db-get-feed file)))
		  (with-temp-buffer
		    (insert-file-contents file-path)
		    (let* ((xml (elfeed-xml-parse-region (point-min)
							 (point-max)))
			   (entries
			    (cl-case(elfeed-feed-type xml)
			      (:atom (elfeed-entries-from-atom file xml))
			      (:rss (elfeed-entries-from-rss file xml))
			      (:rss1.0 (elfeed-entries-from-rss1.0 file xml))
			      (otherwise
			       (error (elfeed-handle-parse-error
				       file
				       "Unknown feed type."))))))
		      (elfeed-db-add entries))))
	      (error (elfeed-handle-parse-error file error)))))
	(run-hook-with-args 'elfeed-update-hooks file))
    ;; return nil if not local file so that elfeed-update-feed can take over
    nil))
(advice-add 'elfeed-update-feed :before-until #'my-elfeed-update-feed-local)


(defvar my-elfeed-feeds nil
  "((category1
  (\"name a\" . \"https://link.a\")
  (\"name b\" . \"https://link.b\"))
 (category2
  (\"name c\" . \"https://link.c\")
  (\"name d\" . \"https://link.d\")))")

(defvar my-elfeed-base-links
  '((bitchute . "https://www.bitchute.com/feeds/rss/channel/%s")
    ;; (youtube . "https://www.youtube.com/feeds/videos.xml?channel_id=%s")
    (youtube . "https://inv.vern.cc/feed/channel/%s")
    ;; (invidious . "https://invidious.snopyta.org/feed/channel/%s")
    (invidious . "https://inv.vern.cc/feed/channel/%s")
    (odysee . "https://odysee.com/$/rss/@%s")
    (nitter . "https://nitter.lucabased.xyz/%s/rss")
    (twitch . "https://twitchrss.appspot.com/vod/%s")))

;; This is a workaround for my current nitter instance being dumb.
(defun my-elfeed-xml-parse-advice (&optional beg end buffer
					     parse-dtd _parse-ns)
  (and beg (goto-char beg))
  (let ((nitter-url (string-remove-suffix
		     "/%s/rss"
		     (alist-get 'nitter my-elfeed-base-links))))
    (with-current-buffer (or buffer (current-buffer))
      (save-excursion
	(while (re-search-forward "https?://nitter\\.net" end t)
	  (replace-match nitter-url)))
      ;; (let ((str (buffer-substring-no-properties beg end)))
      ;; 	(with-current-buffer (get-buffer-create "*elfeed-xml*")
      ;; 	  (insert str "\n\n")))
      )))
(advice-add #'elfeed-xml-parse-region :before #'my-elfeed-xml-parse-advice)

(defun my-elfeed-add-feed (feed-id name service category)
  (let ((current-category
	 (progn (unless (assoc category my-elfeed-feeds)
		  (add-to-list 'my-elfeed-feeds (list category)))
		(assoc category my-elfeed-feeds))))
    (unless (assoc name (cdr current-category))
      (setcdr current-category
	      (cons
	       (cons name
		     (format (or (alist-get service my-elfeed-base-links) "%s")
			     feed-id))
	       (cdr current-category)))))
  my-elfeed-feeds)

(defun my-elfeed-add-feeds (feeds service category)
  "FEEDS is a list of (name . feed-id) pairs.
SERVICE is a symbol from `my-elfeed-base-links'.
CATEGORY is a symbol."
  (dolist (feed feeds)
    (my-elfeed-add-feed (cdr feed) (car feed) service category)))

(defun my-elfeed-enable-category (category)
  "Enable feeds from the certain CATEGORY in `my-elfeed-feeds'."
  (dolist (feed (alist-get category my-elfeed-feeds))
    (cl-pushnew (list (cdr feed) category) elfeed-feeds :test 'equal)))
(defun my-elfeed-enable-categories (&rest categories)
  "Enable feeds from the certain CATEGORIES in `my-elfeed-feeds'."
  (dolist (category categories)
    (my-elfeed-enable-category category)))

;; FIXME: it expects feeds to have only one category
(defun my-elfeed-disable-category (category)
  "Disable feeds from the certain CATEGORY in `my-elfeed-feeds'."
  (dolist (feed (alist-get category my-elfeed-feeds))
    (setq elfeed-feeds (delete (list (cdr feed) category) elfeed-feeds))))
(defun my-elfeed-disable-categories (&rest categories)
  "Disable feeds from the certain CATEGORIES in `my-elfeed-feeds'."
  (dolist (category categories)
    (my-elfeed-disable-category category)))

(defun my-elfeed-disable-by-name (&rest names)
  "Disable singular feeds."
  (dolist (category my-elfeed-feeds)
    (dolist (feed (cdr category))
      (when (member (car feed) names)
	(setq elfeed-feeds (delete (list (cdr feed) (car category))
				   elfeed-feeds))))))

(defun my-elfeed-get-feed-name (url)
  "Return the name of a feed in `my-elfeed-feeds' that matches the URL."
  (catch 'return
    (seq-do (lambda (category)
	      (seq-do (lambda (feed)
			(when (string-equal (cdr feed) url)
			  (throw 'return (car feed))))
		      (cdr category)))
	    my-elfeed-feeds)
    nil))

(defun my-elfeed-get-feed-url (name)
  "Return the url of a feed in `my-elfeed-feeds' that matches the NAME."
  (catch 'return
    (seq-do (lambda (category)
	      (seq-do (lambda (feed)
			(when (string-equal (car feed) name)
			  (throw 'return (cdr feed))))
		      (cdr category)))
	    my-elfeed-feeds)
    nil))

(defun my-elfeed-catchup (regex)
  (interactive
   (list (read-string "Regex: " (elfeed-feed-title
				 (elfeed-entry-feed
				  (car (elfeed-search-selected)))))))
  (when (eq major-mode 'elfeed-search-mode)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward regex nil 'noerror)
	(elfeed-search-untag-all-unread)))))

;; TODO: Create an advice for the function in elfeed that reads elfeed-feeds.
;;       my-elfeed-feeds should store structs with name, url, category and
;;       enabled keywords (or maybe plists with these keywords).
