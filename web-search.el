;;; -*- lexical-binding: t; -*-

;;; Usage:

;; Existing search engines can be redefined by calling `define-search-engine'
;; using the same engine name.

(require 'dom)
(require 'cl-lib)
(require 'seq)

(defvar web-search-engines nil
  "Search engines list.

Check `define-search-engine' for the documentation about the stored values.")

(defvar web-search-browser-function nil
  "Function to perform the search in a WWW browser.

This is used only when the search engine has no PARSE-FUNCTION.
More details in `define-search-engine' docstring.")

(cl-defstruct search-engine name keyword template-uri parse-function)

(defun web-search--lisp-case (s)
  (downcase (replace-regexp-in-string "[ \t\n\r]+" "-" s)))

(defun web-search--maybe-pop-to-buffer (buffer)
  (cond ((bufferp buffer) (pop-to-buffer buffer))
	((stringp buffer) (message buffer))
	(t (message "Search failed."))))

(defun web-search--dom-remove-recursively (predicate dom)
  "Destructively remove all nodes matching PREDICATE from DOM."
  (cl-dolist (child (dom-children dom))
    (when (listp child)
      (if (funcall predicate child)
	  (delq child dom)
	(web-search--dom-remove-recursively predicate child))))
  dom)

(defun web-search--dom-attr-matcher (attr-type regexp)
  "To be used with `web-search--dom-remove-recursively'."
  (lambda (node)
    (let ((current-attr (dom-attr node attr-type)))
      (and current-attr (string-match-p regexp current-attr)))))

(defun web-search--dom-tag-matcher (tag)
  "To be used with `web-search--dom-remove-recursively'."
  (lambda (node) (eq (dom-tag node) tag)))

(defun web-search--dom-remove-by-attr-recursively (attr-type attr dom)
  (web-search--dom-remove-recursively
   (web-search--dom-attr-matcher attr-type attr)
   dom))

(defun web-search--get-absolute-url (url base)
  (unless (string-prefix-p "http" base)
    (setq base (concat "https://" base)))
  (cond ((string-prefix-p "//" url) (concat "https:" url))
	((string-prefix-p "/" url) (concat base url))
	(t url)))

;; TODO: Maybe PARSE-FUNCTION shouldn't take the DOM object as an argument.
;; NOTE  Instead it should take url argument so it would be very simple to
;;       define search engines with PARSE-FUNCTIONs like eww-browse-url
;;       or web-search-browser-function.
;;       In that case how does it differ from setting
;;       `browse-url-default-browser' as a list of (REGEXP . FUNCTION) pairs?
;;       Maybe this file doesn't make sense?
;;;###autoload
(defun define-search-engine (name keyword template-uri parse-function)
  "Define a new search engine.

NAME is the legible name of the search engine.

KEYWORD is the keyword used to quickly select the engine.

TEMPLATE-URI is an URI for the search with \"%s\" in place of a search query.

PARSE-FUNCTION is a function that takes a singular argument that is a DOM
object of an HTML response from retrieving TEMPLATE-URI with the search query.
The return value can be:
- a fully rendered buffer,
- nil if the search was unsuccessfull - in that case the generic message would
  be shown to the user,
- a string with the custom message to be shown in the minibuffer.
The value of PARSE-FUNCTION can also be `nil'. In that case the search will be
performed with `browse-url-browser-function' or `web-search-browser-function' if it's not nil (this will take priority)."
  (cond ((not (stringp name))
	 (error "NAME should be a string"))
	((not (stringp keyword))
	 (error "KEYWORD should be a string"))
	((not (stringp template-uri))
	 (error "TEMPLATE-URI should be a string"))
	;; if parse-function is nil, do not check for it's correctness
	((null parse-function))
	((not (functionp parse-function))
	 (error "PARSE-FUNCTION should be a function"))
	((not (= (car (func-arity parse-function)) 1))
	 (error "PARSE-FUNCTION should take only one argument")))
  (cl-dolist (search-engine web-search-engines)
    (cond
     ;; Remove search engine if the name is already on the list
     ;; FIXME: Corner case: if there is a search engine with the same name
     ;;        it will be deleted and then if the keyword is the same as
     ;;        in the another search engine it will throw an error leaving
     ;;        the list with removed engine but without the replacement
     ;;        I guess it's not the end of the world because the user would
     ;;        like to redefine the engine anyway.
     ((string= (search-engine-name search-engine) name)
      (setq web-search-engines (delq search-engine web-search-engines)))
     ((string= (search-engine-keyword search-engine) keyword)
      (error (format "Keyword `%s' already taken"
  			  keyword)))))
  (push (make-search-engine
       	 :name name
       	 :keyword keyword
       	 :template-uri template-uri
       	 :parse-function parse-function)
       	web-search-engines))

;;;###autoload
(defun web-search (search-string)
  "Search web with a search engine defined in `web-search-engines'.

SEARCH-STRING should be \"KEYWORD QUERY\" where KEYWORD is search engine's
keyword and QUERY is stuff you want to find."
  (interactive "sSearch string: "
	       (list search-string))
  (cl-do* ((first-whitespace (string-match "[ \t\n\r]+" search-string))
	   (se-keyword (substring-no-properties search-string
						0 first-whitespace))
	   (se-query (string-trim (substring-no-properties search-string
							   first-whitespace)))
	   (search-engines web-search-engines (cdr search-engines))
	   (search-engine (car search-engines) (car search-engines)))
      ((or (null search-engines)
	   (string= (search-engine-keyword search-engine) se-keyword))
       (if search-engine
	   (if (search-engine-parse-function search-engine)
	       (web-search--maybe-pop-to-buffer
		(funcall `,(search-engine-parse-function search-engine)
	 		 (with-current-buffer
	 		     (url-retrieve-synchronously
	 		      (format
			       (search-engine-template-uri search-engine)
			       se-query))
	 		   (libxml-parse-html-region (point) (point-max)))))
	     (funcall (or web-search-browser-function
			  browse-url-browser-function)
		      (format (search-engine-template-uri search-engine)
			      se-query)))
	 (message "Keyword doesn't match any search engine.")))))


;; This is left for the reference.
;; Maybe in the future I'd like to create custom parser for resulting DOM
;; instead of shr.
;; (defun web-search--sjp (query)
;;   (let ((results
;; 	 (with-current-buffer (url-retrieve-synchronously
;; 			       (format web-search-sjp-uri-template query))
;; 	   ;; type-187125: Wielki słownik ortograficzny PWN
;; 	   ;; type-187126: Słownik Języka Polskiego PWN
;; 	   ;; type-187123: Słownik języka polskiego pod red. W. Doroszewskiego
;; 	   ;; type-187386: Porady językowe
;; 	   ;; type-187142: Encyklopedia PWN
;; 	   (dom-by-class (libxml-parse-html-region (point-min)
;; 						   (point-max))
;; 			 "type-187126"))))

;;     (with-current-buffer
;; 	(get-buffer-create (format "*web-search-sjp: %s*" query))
;;       (cl-dolist ((entry results))
;; 	(insert (dom-text (dom-by-tag (dom-by-class entry "tytul")
;; 				      'a)))
;; 	(insert (string-trim (replace-regexp-in-string
;; 			      "•\\| " (dom-text entry))))))))

;; TODO: Move those to init.el or create a file for web-search configuration

(setq web-search-browser-function 'eww-browse-url)

(define-search-engine
  "Słownik Języka Polskiego PWN"
  "s"
  "https://sjp.pwn.pl/szukaj/%s"
  (lambda (dom)
    (setq dom (dom-by-class dom "search-content"))
    (if (dom-by-class dom "wyniki")
	(with-current-buffer
	    (get-buffer-create "*web-search-sjp*")
	  (read-only-mode -1)
	  (erase-buffer)
	  (web-search--dom-remove-recursively
	   (web-search--dom-tag-matcher 'img)
	   dom)
	  (shr-insert-document dom)
	  (goto-char (point-min))
	  (view-mode 1)
	  (local-set-key (kbd "q") #'kill-this-buffer)
	  (current-buffer))
      nil)))

;; (define-search-engine
;;   "Oxford Dictionary"
;;   "o"
;;   "https://www.lexico.com/en/definition/%s"
;;   (lambda (dom)
;;     (setq dom (dom-by-class dom "entryWrapper"))
;;     (with-current-buffer
;; 	(get-buffer-create "*web-search-oxford*")
;;       (read-only-mode -1)
;;       (erase-buffer)
;;       (shr-insert-document dom)
;;       (goto-char (point-min))
;;       (view-mode 1)
;;       (local-set-key (kbd "q") #'kill-this-buffer)
;;       (current-buffer))))

(define-search-engine
  "Oxford Learner's Dictionary"
  "o"
  "https://www.oxfordlearnersdictionaries.com/definition/english/%s"
  (lambda (dom)
    (setq dom (dom-by-id dom "entryContent"))
    (web-search--dom-remove-recursively
     (web-search--dom-attr-matcher 'id "ring-links-box")
     dom)
    (with-current-buffer
	(get-buffer-create "*web-search-oxford*")
      (read-only-mode -1)
      (erase-buffer)
      (shr-insert-document dom)
      (goto-char (point-min))
      (view-mode 1)
      (local-set-key (kbd "q") #'kill-this-buffer)
      (current-buffer))))

(defun web-search-glosbe (dom)
  (setq dom (ignore-error 'wrong-type-argument
	      (dom-parent
	       dom (dom-parent
		    dom (car (dom-by-id dom "dictionary-content"))))))
    (if (or (not dom) (dom-by-class dom "alert"))
	nil
      (with-current-buffer
	  (get-buffer-create "*web-search-glosbe*")
	(read-only-mode -1)
	(erase-buffer)
	(web-search--dom-remove-recursively
	 (web-search--dom-tag-matcher 'img)
	 dom)
	;; (web-search--dom-remove-by-attr-recursively 'class "dropdown-menu" dom)
	(shr-insert-document dom)
	(goto-char (point-min))
	(view-mode 1)
	(keymap-local-set (kbd "q") #'kill-this-buffer)
	(current-buffer))))

(define-search-engine
  "Glosbe Translate Pl-En"
  "tpe"
  "https://glosbe.com/pl/en/%s"
  #'web-search-glosbe)

(define-search-engine
  "Glosbe Translate En-Pl"
  "tep"
  "https://glosbe.com/en/pl/%s"
  #'web-search-glosbe)

(define-search-engine
  "Glosbe Translate Pl-Fr"
  "tpf"
  "https://glosbe.com/pl/fr/%s"
  #'web-search-glosbe)

(define-search-engine
  "Glosbe Translate Fr-Pl"
  "tfp"
  "https://glosbe.com/fr/pl/%s"
  #'web-search-glosbe)

(define-search-engine
  "Glosbe Translate Ja-Pl"
  "tjp"
  "https://glosbe.com/ja/pl/%s"
  #'web-search-glosbe)

(define-search-engine
  "Glosbe Translate Pl-Ja"
  "tpj"
  "https://glosbe.com/pl/ja/%s"
  #'web-search-glosbe)

(define-search-engine
  "Wikipedia PL"
  "w"
  "https://pl.wikipedia.org/wiki/Special:Search?search=%s"
  nil)

;; TODO: This could be left in this file as a default and the reference for
;;       how to create an engine.
(define-search-engine
  "Wikipedia EN"
  "we"
  "https://en.wikipedia.org/wiki/Special:Search?search=%s"
  nil)


(define-search-engine
  "Synonimy PL"
  "sy"
  "https://synonim.net/synonim/%s"
  (lambda (dom)
    (with-current-buffer
	(get-buffer-create "*web-search-synonimy*")
      (read-only-mode -1)
      (erase-buffer)
      (shr-insert-document (car (dom-by-id dom "main")))
      (goto-char (point-min))
      (view-mode 1)
      (local-set-key (kbd "q") #'kill-this-buffer)
      (current-buffer))))


;; jisho.org - japanese dictionary
;; This is an attempt at parsing the website and using my own rendering of
;; the data collected.
;; It will all crumble down when jisho.org decides to change their website.

(defcustom web-search-jisho-separator "\n\n⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\n\n"
  "Separator string inserted between entries."
  :type '(string)
  :group 'web-search)

(cl-defstruct web-search-jisho-word
  reading				; clas: concept_light-tag
  tags					; class: concept_light-status
  audio					; tag: audio, id: audio_.*
  collocations				; id: links_coll_.*
  links					; id: links_drop_.*
  meanings				; class: concept_light-meanings
  details-link				; class: light-details_link
  )

;; call shr-insert-document on meanings

;; [id: main_results] stores the results
;; [id: primary] stores the Words column
;; [id: secondary] is for [class: kanji_light_block], [class: names_block]
;;   and supposedly other stuff (like [id: other_dictionaries] with links to
;;   external sites)

(defun web-search-insert-vp (&rest args)
  (let ((start (point)))
    (apply #'insert args)
    (put-text-property start (point) 'face 'variable-pitch)))

(defun web-search--jisho-kana-p (obj)
  "Check if OBJ (character or string) is a hiragana or katakana symbol."
  (require 'japan-util)
  (cl-find-if (lambda (group) (member obj group)) japanese-kana-table))

(defun web-search--jisho-render-reading (reading)
  (let ((start (point)))
    (web-search-insert-vp (cdr reading))
    (add-face-text-property start (point) '(:height 240)))
  (let ((spelling
	 (cl-loop for pron in (car reading)
		  for spell across (cdr reading)
		  concat (cond ((not (string-empty-p pron))
				pron)
			       ((or (web-search--jisho-kana-p spell)
				    (char-equal ?ー spell))
				(char-to-string spell))))))
    (when spelling (insert "【" spelling "】"))))

(cl-defmethod web-search--jisho-render ((word web-search-jisho-word))
  (let ((readings (web-search-jisho-word-reading word))
	(audio (web-search-jisho-word-audio word))
	(collocations (web-search-jisho-word-collocations word))
	(links (web-search-jisho-word-links word)))
    ;; Spelling with kanji
    (let ((elt (car readings))
	  (rest (cdr readings)))
      (while rest
	(web-search--jisho-render-reading elt ", ")
	(setq elt (car rest)
	      rest (cdr rest)))
      (web-search--jisho-render-reading elt))
    ;; Audio
    (when audio (web-search-insert-vp " " (string-join audio " ")))
    ;; Tags
    (web-search-insert-vp
     "\n\nTags: " (string-join (web-search-jisho-word-tags word) ", "))
    ;; Meanings
    (web-search-insert-vp "\n\n" (web-search-jisho-word-meanings word) "\n")
    ;; Collocations
    (when collocations (web-search-insert-vp "Collocations:\n\t"
			       (string-join collocations "\n\t") "\n\n"))
    ;; External links
    (when links (web-search-insert-vp "Links:\n\t"
			(string-join links "\n\t") "\n\n"))
    ;; Link to details
    (web-search-insert-vp (web-search-jisho-word-details-link word))))

(defun web-search-jisho (dom)
  (with-current-buffer
      (get-buffer-create "*web-search-jisho*")
    (read-only-mode -1)
    (erase-buffer)
    (shr-insert-document (dom-by-id dom "the_corrector"))
    (web-search-insert-vp "\n")
    (mapc (lambda (entry)
	    (web-search--jisho-render entry)
	    (web-search-insert-vp web-search-jisho-separator))
	  (web-search--jisho-collect dom))
    (goto-char (point-min))
    (view-mode 1)
    (local-set-key (kbd "q") #'kill-this-buffer)
    (current-buffer)))

(defun web-search--jisho-parse-reading (node)
  "Return a pair with a car being the pronunciation in hiragana as a list,
with every element being a representation of a single character of the cdr,
which is a string and represents the correct spelling of the word."
  (cl-assert (string= "concept_light-representation" (dom-attr node 'class)))
  ;; The car of this is a pronunciation in hiragana for every character from
  ;; the cdr.
  ;; Every span in car is either empty if there's no pronunciation specified,
  ;; or has a hiragana string with a class attribute of "kanji-X-up", where
  ;; X is the number of hiragana characters used for this one kanji.
  ;; NOTE: It seems that sometimes the website reports the pronunciation in
  ;;       the wrong place (i.e. above the wrong kanji).
  (cons
   (mapcar
    #'dom-text
    (seq-filter (lambda (child) (and (consp child) (eq (car child) ' span)))
		(dom-children (car (dom-by-class node "furigana")))))
   (replace-regexp-in-string "[[:space:]\n]" ""
			     (dom-texts (dom-by-class node "text")))))

;; TODO: Propertize the tag depending on the class? Seems like there are only
;;       normal tags and those with "success" in their class, which are
;;       colorized green.
(defun web-search--jisho-parse-tag (node)
  (cl-assert (string-match-p "concept_light-tag" (dom-attr node 'class)))
  (let ((link (dom-child-by-tag node 'a)))
    (if link
	(button-buttonize (dom-text link) #'browse-url
			  (web-search--get-absolute-url
			   (dom-attr link 'href) "https://jisho.org"))
      (dom-text node))))

(defun web-search--jisho-play-sound (url)
  (start-process "*web-search-jisho-sound*" nil
		 "mpv" "--no-config" "--no-video" url))

(let ((order '("audio/ogg"
	       "audio/mpeg")))
  (defun web-search--jisho-parse-audio-pick-source (node)
    (web-search--get-absolute-url
     (dom-attr
      (car (sort
	    (dom-by-tag node 'source)
	    (lambda (a b)
	      (< (or (cl-position (dom-attr a 'type) order :test #'string=)
		     100)
		 (or (cl-position (dom-attr b 'type) order :test #'string=)
		     100)))))
      'src)
     "https://jisho.org")))

(defun web-search--jisho-parse-audio (node)
  (cl-assert (and (consp node) (eq (car node) 'audio)))
  (button-buttonize
   "Play audio"
   #'web-search--jisho-play-sound
   (web-search--jisho-parse-audio-pick-source node)))

(defun web-search--jisho-parse-url-list (node)
  (cl-assert (string-match-p "^links_" (dom-attr node 'id)))
  (mapcar (lambda (a)
	    (let ((url (dom-attr a 'href)))
	      (button-buttonize
	       (dom-text a)
	       #'browse-url
	       (web-search--get-absolute-url url "https://jisho.org"))))
	  (dom-by-tag node 'a)))

;; TODO: I need to make a function that will render a specific kana with
;;       a hiragana spelling above.

;; TODO:
(defun web-search--jisho-parse-meanings (node)
  (with-temp-buffer
    (shr-insert-document node)
    (buffer-string)))

(defun web-search--jisho-parse-details-link (node)
  (button-buttonize "Details ▸" #'browse-url
		    (web-search--get-absolute-url
		     (dom-attr node 'href) "https://jisho.org")))

;; TODO: Add inflections. Seems like they're generated dynamically, using
;;       data-pos attribute of [class: show_inflection_table].
;;       The code for it can be found in the jisho js script, just search for
;;       InflectionPattern.
(cl-flet ((and=> (value proc) (when value (funcall proc value))))
  (defun web-search--jisho-parse-word (node)
    (cl-assert (string= "concept_light clearfix" (dom-attr node 'class)))
    (let ((reading (mapcar
		    #'web-search--jisho-parse-reading
		    (dom-by-class node "concept_light-representation")))
	  (tags (mapcar #'web-search--jisho-parse-tag
			(dom-by-class node "concept_light-tag")))
	  (audio (mapcar #'web-search--jisho-parse-audio
			 (dom-by-tag node 'audio)))
	  (collocations (and=> (car (dom-by-id node "^links_coll_"))
			       #'web-search--jisho-parse-url-list))
	  (links (and=> (car (dom-by-id node "^links_drop_"))
			#'web-search--jisho-parse-url-list))
	  (meanings (web-search--jisho-parse-meanings
		     (car (dom-by-class node "concept_light-meanings"))))
	  (details-link (web-search--jisho-parse-details-link
			 (car (dom-by-class node "light-details_link")))))
      (make-web-search-jisho-word :reading reading
				  :tags tags
				  :audio audio
				  :collocations collocations
				  :links links
				  :meanings meanings
				  :details-link details-link))))

(defun web-search--jisho-collect (dom)
  (let* ((main-results (dom-by-id dom "main_results"))
	 (words (dom-by-class (dom-by-id main-results "primary")
			      "concept_light clearfix"))
	 (secondary (dom-by-id main-results "secondary"))
	 (kanji (dom-by-class secondary "kanji_light_block"))
	 (names (dom-by-class secondary "names_block")))
    (mapcar #'web-search--jisho-parse-word words)
    ;; TODO: parse also kanji and names
    ))

;; (pp-display-expression
;;  my-temp
;;  ;; (web-search--jisho-collect my-temp)
;;  "*pp*")

(define-search-engine "Jisho"
  "ja"
  "https://jisho.org/search/%s"
  #'web-search-jisho)


(define-search-engine "Urban Dictionary"
  "u"
  "https://www.urbandictionary.com/define.php?term=%s"
  (lambda (dom)
    (with-current-buffer
	(get-buffer-create "*web-search-urbandictionary*")
      (read-only-mode -1)
      (erase-buffer)
      (shr-insert-document (dom-by-tag dom 'section))
      (goto-char (point-min))
      (view-mode 1)
      (local-set-key (kbd "q") #'kill-this-buffer)
      (current-buffer))))
