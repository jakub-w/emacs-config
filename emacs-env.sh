# FIXME:
# <sadbear> I'm using this function https://bpa.st/SMYA to try to oppen
# 	    emacsclient in the existing frame or create a new frame and open
# <sadbear> if I just use `em` it works fine but `em file1 file2 ...` fails
# <sadbear> This function is provided on emacswiki.org https://bpa.st/SMYA but
# 	    it doesn't open a window when em is called with args.
# Could it be that it doens't work if the server isn't already running? Or
# is it about passing multiple files?

em()
{
    args=""
    nw=false
    # check if emacsclient is already running
    if pgrep -U $(id -u) emacsclient > /dev/null; then running=true; fi

    # check if the user wants TUI mode
    for arg in "$@"; do
    	[ "$arg" = "-nw" ] || [ "$arg" = "-t" ] || [ "$arg" = "--tty" ] && \
	    nw=true
    done

    # if called without arguments - open a new gui instance
    [ "$#" -eq "0" ] || [ "$running" != true ] && args=(-c $args)
    if [ "$#" -gt "0" ]; then
	# if 'em -' open standard input (e.g. pipe)
	if [[ "$1" == "-" ]]; then
    	    TMP="$(mktemp /tmp/emacsstdin-XXX)"
    	    cat >$TMP
	    args=($args --eval \
			'(let ((b (generate-new-buffer "*stdin*")))
			   (switch-to-buffer b)
			   (insert-file-contents "'${TMP}'")
			   (delete-file "'${TMP}'")
			   (x-urgent))')
	else
	    args=($@ $args)
	fi
    fi

    # emacsclient $args
    $nw && emacsclient "${args[@]}" || \
	    (nohup emacsclient "${args[@]}" > /dev/null 2>&1 &) > /dev/null
}

emc() # command line emacs
{
    em -nw $@
}

alias emm="emacs -q -nw --load ~/.emacs.d/minimal-init.el"
