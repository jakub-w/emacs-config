;; -*- lexical-binding: t -*-

(require 'seq)
(require 'cl-lib)

(defvar media--vorbis-exts '("ogg"))
(defvar media--flac-exts '("flac"))
(defvar media--id3-exts '("mp3"))
(defvar media--opus-exts '("opus"))
(defvar media--tags-dispatch
  `((,(regexp-opt media--vorbis-exts) :set media--set-vorbis-tags)
    (,(regexp-opt media--id3-exts) :set media--set-id3-tags)
    (,(regexp-opt media--flac-exts) :set media--set-flac-tags)
    (,(regexp-opt media--opus-exts) :set media--set-opus-tags))
  "An alist. The first value is a key in the form of a regex to match.
The cdr of every entry is a plist of setters (:set) and getters (:get).")

(defun media--check-extension (ext-list filename)
  (unless (member (file-name-extension filename) ext-list)
    (user-error "Wrong file type" filename)))

;; FIXME: Don't set unspecified tags
(defun media--set-vorbis-tags (artist album year title track-num filename)
  (setq filename (shell-quote-argument filename))
  (media--check-extension media--vorbis-exts filename)
  (unless (executable-find "vorbiscomment")
    (user-error "Could not find vorbiscomment"))
  (let ((tags (mapcar (lambda (tag)
			(string-match "\\([^=]+?\\)=\\(.*\\)" tag)
			(cons (upcase (match-string-no-properties 1 tag))
			      (match-string-no-properties 2 tag)))
		      (process-lines "vorbiscomment" "--list" filename))))
    (dolist (tag `(("ARTIST" . ,artist)
    		   ("ALBUM" . ,album)
    		   ("YEAR" . ,year)
    		   ("TITLE" . ,title)
    		   ("TRACKNUMBER" . ,track-num)))
      (setf (alist-get (car tag) tags nil nil #'string=) (cdr tag)))
    (shell-command
     (concat "vorbiscomment --raw --write "
	     (mapconcat
	      (lambda (tag)
		(if (cdr tag)
		    (format "--tag '%s=%s'" (car tag) (cdr tag))
		  ""))
	      tags " ")
	     " '" filename "'"))))

(defun media--set-flac-tags (artist album year title track-num filename)
  (media--check-extension media--flac-exts filename)
  (unless (executable-find "metaflac")
    (user-error "Could not find metaflac"))
  (let ((args
	 (append
	  (if artist `("--remove-tag=ARTIST"
		       ,(format "--set-tag=ARTIST=%s" artist)))
	  (if album `("--remove-tag=ALBUM"
		      ,(format "--set-tag=ALBUM=%s" album)))
	  (if year `("--remove-tag=YEAR"
		     ,(format "--set-tag=YEAR=%s" year)))
	  (if title `("--remove-tag=TITLE"
		      ,(format "--set-tag=TITLE=%s" title)))
	  (if track-num `("--remove-tag=TRACKNUMBER"
			  ,(format "--set-tag=TRACKNUMBER=%s" track-num))))))
    (apply #'call-process "metaflac" nil nil nil filename args)))

(defun media--set-id3-tags (artist album year title track-num filename)
  (media--check-extension media--id3-exts filename)
  (let ((id3-exec
	 (or (executable-find "id3tag")
	     (executable-find "id3v2")
	     (user-error "Could not find neither id3tag nor id3v2")))
	(args (append (if artist `("--artist" ,artist))
		      (if album `("--album" ,album))
		      (if year `("--year" ,(format "%s" year)))
		      (if title `("--song" ,title))
		      (if track-num `("--track" ,(format "%s" track-num)))
		      (list filename))))
    (apply #'call-process id3-exec nil nil nil args)))

(defun media--set-opus-tags (artist album year title track-num filename)
  (media--check-extension media--opus-exts filename)
  (let ((opus-exec (or (executable-find "ffmpeg")
		       (user-error "Could not find ffmpeg")))
	(temp-file (format "%s.opus" (make-temp-name "/tmp/music-file-")))
	(args (append `("-i" ,filename)
		      (if artist `("-metadata" ,(format "artist=%s" artist)))
		      (if album `("-metadata" ,(format "album=%s" album)))
		      (if year `("-metadata" ,(format "date=%s" year)))
		      (if title `("-metadata" ,(format "title=%s" title)))
		      (if track-num `("-metadata" ,(format "tracknumber=%s"
							   track-num)))
		      `("-codec" "copy" ,temp-file))))
    (when (= 0 (apply #'call-process opus-exec nil "*my-test-ffmpeg*" nil
		      args))
      (copy-file temp-file filename t)
      (delete-file temp-file)
      t)))

(defun media-set-tags (artist album year title track-num filename)
  "Set id3 tags, vorbis comments or flac metadata of a music file."
  (setq filename (expand-file-name (substitute-in-file-name filename)))
  (let ((plist (assoc-default (file-name-extension filename)
			      media--tags-dispatch
			      #'string-match-p)))
    (if (not plist)
	(user-error "[media-set-tags] No known method to set tags")
      (funcall (plist-get plist :set)
	       artist album year title track-num filename))))

(defun media-extract-from-file (filename from to outfile &optional then)
  "Asynchronously extract a part of the file FILENAME designated by the time
values FROM and TO.  Save the result as OUTFILE.

Optional argument THEN should be a function taking a parameter to which
OUTFILE will be passed.  The function will be evaluated after this function
finishes (only if the extraction succeeded)."
  (let ((buffer (generate-new-buffer " *ffmpeg-extract*")))
    (make-process :name "*ffmpeg-extract*"
		  :buffer buffer
		  :command
		  (list "ffmpeg"
			"-i" filename
			"-acodec" "copy"
			"-ss" (format "%s" from)
			"-to" (format "%s" to)
			outfile)
		  :sentinel
		  (lambda (process event)
		    (unless (process-live-p process)
		      (if (= (process-exit-status process) 0)
			  (when then
			    (funcall then outfile)
			    (kill-buffer buffer))
			(pop-to-buffer buffer)))))))

(defun media-split-album (artist album year songlist input-file)
  "Split one-file album to multiple files based on SONGLIST.

Also set id3 tags in the output files using \"id3tag\" command.

SONGLIST needs to have a following format:
  ((1 \"Song 1 name\" \"0:00\" \"4:08\")
   (2 \"Song 2 name\" \"4:08\" \"8:03\")
   ... )
So every entry is: (<number> \"<name>\" \"<start-time>\" \"<end-time>\")"
  (unless (file-exists-p input-file)
    (user-error "[media-split-album] File doesn't exist: %s" input-file))
  (with-temp-buffer
    (dolist (song songlist)
      (let* ((title (nth 1 song))
	     (track-num (nth 0 song))
	     (from (nth 2 song))
	     (to (nth 3 song))
	     (outdir (format "%s(%d) %s/"
			     (file-name-directory input-file) year album))
	     (outfile (format "%s%02d. %s.%s"
			      outdir
			      track-num
			      title
			      (file-name-extension input-file))))
	(mkdir outdir t)
	(media-extract-from-file
	 input-file from to outfile
	 (lambda (outfile)
	   (media-set-tags artist album year title track-num outfile)))))))

(defun media-split-flac (cue-file flac-file)
  (cl-assert (and (executable-find "shnsplit")
		  (executable-find "cuetag.sh")))
  (with-temp-buffer
    (insert-file-contents cue-file)
    (let* ((year (ignore-error 'search-failed
		   (progn
		     (goto-char (point-min))
		     (search-forward-regexp "DATE \\([[:digit:]]\\{4\\}\\)")
		     (match-string 1))))
	   (album
	    (progn (goto-char (point-min))
		   (search-forward-regexp "TITLE \"\\(.+\\)\"$")
		   (match-string 1)))
	   (outdir (format "%s(%s) %s/"
			  (file-name-directory flac-file)
			  year
			  album)))
      (make-directory outdir t)
      (erase-buffer)
      (unless (= 0 (call-process "shnsplit" nil t nil
				 "-f" cue-file
				 "-t" "%n. %t"
				 "-d" outdir
				 "-o" "flac"
				 flac-file))
	(error "Couldn't split the flac file: %s" (buffer-string)))
      (erase-buffer)
      (unless (= 0 (apply #'call-process "cuetag.sh" nil t nil
			  cue-file
			  (seq-remove
			   (lambda (filename)
			     (let ((filename (file-name-nondirectory
					      filename)))
			       (or (string-prefix-p "00." filename)
				   (string-prefix-p "0." filename))))
			   (directory-files outdir t "^[^.].*"))))
	(error ("Couldn't apply the tags to resulting flac files: %s"
		(buffer-string)))))))


;; FIXME: This macro has issues. Without quoting it tries to evaluate lists as
;;        procedures and with quoting it doesn't resolve variables.
(defmacro splicing-eval (form)
  (let ((instructions))
    (mapc (lambda (subform)
	    (cond
	     ((and (consp subform) (eq '@ (car subform)))
	      (push `(when ,(cadr subform) (push ,(caddr subform) new-form))
		    instructions))
	     ((consp subform)
	      (push `(push ,subform new-form)
		      instructions))
	     (t (push `(push (quote ,subform) new-form)
		      instructions))))
	  form)
    `(eval (let ((new-form))
	     ,@instructions
	     new-form))))

;; NOTE:
;; Exec-list should be (("executable1" . ("--artist" "--song" ...))
;;                      ("executable2" . ("--permormer" "--title" ...))
;; Also a second exec-list for obtaining metadata.
;; The resulting macro should create a function taking keyword arguments
;; aside from the filename.

;; TODO: There should be an argument for a method to obtain the metadata
;;       from the file.
;; (defmacro define-metadata-method (name exec-list
;; 				       artist album year title track-num)
;;   "NAME is the method name.

;; EXEC-LIST is an alist of external executables that are needed for the method
;; with a generic template of arguments required for a metadata. Variables
;; KEY and VALUE are available in the metadata template and will be replaced by
;; their corresponding string values.
;; For example, a command for id3tag looks like this:
;; id3tag --artist \"Foo\" filename.mp4
;; The entry for that command should be:
;; (\"id3tag\" . `(,(concat \"--\" key) value)

;; The rest of the arguments are metadata key strings for each type of metadata
;; for the method."
;;   (letrec ((substitute
;; 	    (lambda (old new seq)
;; 	      (cond )))))
;;   `(defun* ,(make-symbol (concat "media--set-" (symbol-name name) "-tags"))
;;        (filename &key artist title album year track-num)
;;      (pcase-let ((`(,exec . ,template)
;; 		  (seq-find (lambda (entry)
;; 			      (executable-find (car entry)))
;; 			    ,exec-list)))
;;        (let ((args '())
;; 	     (artist-local artist))
;; 	 (when artist (push (substitute ,artist 'key template)
;; 			    args))
;; 	 args)
;;        template
;;        ;; `(call-process ,exec nil nil nil
;;        ;; ,template)
;;        ;; template

;;        )))

;; (pp-eval-expression
;;  '(macroexpand '(define-metadata-method foo '(("id3tag" . `(,(concat "--" key)
;; 							    value)))
;; 		  "Artist" "Album" "Year" "Title" "Track-Num")))

;; (define-metadata-method foo '(("foo" . (lala)))
;;   "artist" "album" "year" "title" "track-num")

;; (pp-eval-expression
;;  '(media--set-foo-tags "lol.mp4" :artist "Lel" :title "Foo"))


;; TODO: the fields in methods for mp3 and opus should be optional
;; Maybe there should be a generic function or a macro that will simplify
;; adding new methods and reduce code redundancy.
;; Something that will take a list of arguments for a given metadata (e.g.
;; artist) and splice it into the argument list for the process call.
;; That way it should be possible to not reimplement code for checking
;; if any given argument is nil or not for every method.
