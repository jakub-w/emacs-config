;; -*- lexical-binding: t -*-
;; THIS FILE SHOULD NOT BE EVALUATED
;; It stores non-used functions as examples and reference

(defun M-x-other-window ()
      "Call M-x in the other window."
      (interactive)
      (save-selected-window
        (other-window 1)
        (execute-extended-command nil)))

(defun nmapcar (proc lst)
  "Like mapcar but destructive (modifies the list in-place)."
  (let ((tail lst))
    (while tail
      (setcar tail (funcall proc (car tail)))
      (setq tail (cdr tail))))
  lst)

(defun my-pairs (lst)
  "Split list LST into pairs."
  (cl-check-type lst list)
  (let ((result))
    (while lst
      (when (cadr lst)
	  (setq result (cons (cons (car lst) (cadr lst)) result)))
      (setq lst (cddr lst)))
    result))


(require 'cl-lib)
(require 'subr-x)
(cl-defun my-id3tag (filename &key artist album song number)
  "Set id3 tags of an mp3 file."
  (unless (file-name-absolute-p filename)
    (user-error "[id3tag] File name must be absolute"))
  (unless (string= (file-name-extension filename) "mp3")
    (user-error "[id3tag] Not an mp3 file"))
  (setq filename (file-truename filename))
  (let ((id3-exec
	 (or (executable-find "id3v2")
	     (executable-find "id3tag")
	     (user-error "[id3tag] Could not find id3tag and id3v2")))
	(id3iconv-exec
	 (or (executable-find "mid3iconv")
	     (user-error "[id3tag] Could not find any id3iconv utility")))
	(args))
    (when artist (setq args (nconc (list "--artist" artist) args)))
    (when album (setq args (nconc (list "--album" album) args)))
    (when song (setq args (nconc (list "--song" song) args)))
    (when number (setq args (nconc (list "--track" number) args)))
    (setq args (nconc args (list filename)))
    (with-temp-buffer
      (unless (= 0 (apply #'call-process id3-exec nil t nil args))
	(user-error (format "[id3tag] %s" (string-trim (buffer-string)))))
      (erase-buffer)
      (unless (= 0 (call-process id3iconv-exec nil t nil filename))
      	(user-error (format "[id3tag] %s" (string-trim (buffer-string))))))))

;; IN-PROGRESS

;; (defun my/c++-new-class (class-name)
;;   (interactive (let ((class-name (read-string "Class name: ")))
;; 		 (list class-name)))
;;   (let ((project-root (my/c++-find-project-root)))
;;     ))


;; TODO: if process finished before setting a sentinel, just run BODY
(defmacro my-eval-after-process (proc &rest body)
  "Evaluate BODY after PROC finished.

Doesn't block the main process.

PROC must be a process."
  (let ((process (cond ((processp proc) proc)
		       ((consp proc) (eval proc)))))
    (unless (processp process)
      (signal 'wrong-type-argument '("PROC must be a process")))
    (set-process-sentinel
     process
     `(lambda (proc msg)
	;; pass to original sentinel
	(funcall #',(process-sentinel process) proc msg)
	(when (string-equal msg "finished\n")
	  ,@body))))
  nil)
;; (defun my-wait-for-process (process)
;;   "Wait for asynchronous process to finish."
;;   (assert (processp process))
;;   (set-process-sentinel
;;    process
;;    `(lambda (proc msg)
;;       ;; pass to original sentinel
;;       (funcall #',(process-sentinel process) proc msg)
;;       (when (string-equal msg "finished\n")
;; 	(message "fin"))))
;;   nil)


(defun my-get-cppreference ()
  (let ((url-template "https://en.cppreference.com/mwiki/api.php?action=query&list=allpages&aplimit=500&apcontinue&format=xml&apfrom=%s")
       (apfrom "")
       (buf (get-buffer-create "*page-list*")))
    (loop
     for url = (format url-template apfrom)
     for xml = (car (with-current-buffer (url-retrieve-synchronously url)
		      (xml-parse-region)))
     do
     (setq apfrom (xml-get-attribute
		   (xml-query '(query-continue allpages) xml)
		   'apcontinue))
     (with-current-buffer buf
       (dolist (page (xml-query-all '(query allpages p) xml))
	 (insert (format "%s\n" (xml-get-attribute page 'title)))))
     until (string= "" apfrom))))
;; (my-get-cppreference)

;; (xml-get-attribute (xml-query '(query-continue allpages) (car (with-current-buffer (url-retrieve-synchronously "https://en.cppreference.com/mwiki/api.php?action=query&list=allpages&aplimit=500&apcontinue&format=xml&apfrom=old/wiki/string/c/strtok") (xml-parse-region))))
;; 'apcontinue)


;; REGEX for variables:
;; "\\<\\(?1:[[:word:]-_<>: ]*\\)\
;; \\(?:\s+\\(?2:[*&]+\\)?\\|\\(?2:[*&]+\\)\s+\\)\
;; \\(?3:[[:word:]-_]+\\)\
;; \s?[=(]?"

;; TEST CASES:
;; unsigned int foo_;
;; std::vector<unsigned int> bar;
;; char* foo;
;; char *foo;
;; std::string& foo;
;; const std::string& foo;

;; int foo = 5;
;; std::string foo = "blabla";
;; std::string foo("blabla");
;; This is an elisp version of dmenu's fuzzy sort
;; TODO: put it somehow into flx; OR make it self-sufficient and fast with
;;       caches and pre-processing
(defun my-sort (text cands)
  "Fuzzy sort.

The algorithm is dmenu's fuzzy sort."
  ;; iterate over candidates
  (condition-case nil
      (mapcar
       'car ; return only the resulting list without the sorting info
       (sort
	(cl-do* ((matches (list))
		 (text-len (length text))
		 (candidates cands (cdr candidates))
		 (candidate (car candidates) (car candidates))
		 (candidate-len (length candidate) (length candidate))
		 (start-idx -1 -1)
		 (end-idx -1 -1))
	    ((null candidates) matches)
	  (if (> text-len 0)
	      (progn
		;; iterate over chars in candidate
		(cl-do* ((cand-idx 0 (1+ cand-idx))
			 (text-idx 0)
			 (c nil))
		    ((or (= cand-idx candidate-len)
			 (= text-idx text-len)))
		  (setq c (elt candidate cand-idx))
		  (when (char-equal c (elt text text-idx))
		    (when (= start-idx -1)
		      (setq start-idx cand-idx))
		    (setq text-idx (1+ text-idx))
		    (when (= text-idx text-len)
		      (setq end-idx cand-idx))))
		;; build list of matches
		(unless (= end-idx -1)
		  ;; compute distance
		  ;; add penalty if match starts late
		  ;; add penalty for a long match without many matching
		  ;;     characters
		  (push (cons candidate
			      (+ (log (+ start-idx 2))
				 (- end-idx start-idx text-len)))
			matches)))
	    (push (cons candidate 0) matches)))
	;; sorting lambda
	(lambda (match1 match2)
	  (<= (cdr match1) (cdr match2)))))
    (error cands)))


(let ((futures (make-hash-table :weakness 'key))
      (num -1))
  (defun my-future--make (thread)
    (let ((mutex (make-mutex)))
      (list thread mutex (make-condition-variable mutex) 'no-value)))
  (defun my-future--thread (internal-future)
    (car internal-future))
  (defun my-future--mutex (internal-future)
    (cadr internal-future))
  (defun my-future--cond-var (internal-future)
    (caddr internal-future))
  (defun my-future--value (internal-future)
    (car (cadddr internal-future)))
  (defun my-future--has-value-p (internal-future)
    (listp (cadddr internal-future)))
  (defun my-future--set-value (internal-future value)
    (setcar (cdddr internal-future) (list value)))
  (defun my-future (function)
    (let ((key (setq num (1+ num))))
      (puthash key
	       (my-future--make
		(make-thread
		 (let ((id key))
		   (lambda ()
		     (let* ((result (funcall function))
			    (f (gethash id futures)))
		       (with-mutex (cadr f)
			 (my-future--set-value f result)
			 (condition-notify (my-future--cond-var f))))))))
	       futures)
      key))
  (defun my-future-has-value-p (future)
    (let ((f (gethash future futures)))
      (with-mutex (my-future--mutex f)
	(my-future--has-value-p f))))
  (defun my-future-value (future)
    (let ((f (gethash future futures)))
      (with-mutex (my-future--mutex f)
	(while (not (my-future--has-value-p f))
	  (condition-wait (my-future--cond-var f)))
	(my-future--value f))))
  (defun my-future-internal (future)
    (gethash future futures))
  (defun my-future-futures ()
    futures))


(setq my-temp-fut (my-future (lambda () (+ 7 12))))
(my-future-value my-temp-fut)
(my-future-futures)

;; TODO: This could be improved by switching to insertion sort when the array
;;       is small enough. Or use introsort (though it's unstable).
(defun my-merge-sort--internal (b e array predicate)
  (let ((distance (abs (- b e))))
    (cond
     ((> distance 1)
      (let* ((middle (+ b (/ distance 2)))
	     (first (my-merge-sort--internal b middle array predicate))
	     (second (my-merge-sort--internal (1+ middle) e array predicate))
	     (result '()))
	(while (and first second)
	  (if (funcall predicate (car first) (car second))
	      (setq result (cons (car first) result)
		    first (cdr first))
	    (setq result (cons (car second) result)
		  second (cdr second))))
	(nconc
	 (nreverse result)
	 (or first second))))
     ((= distance 1)
      (let ((bv (aref array b))
	    (ev (aref array e)))
	(if (funcall predicate bv ev)
	    (list bv ev)
	  (list ev bv))))
     (t (list (aref array b))))))

(defun my-merge-sort (array &optional predicate)
  (cl-check-type array sequence)
  (and predicate (cl-check-type predicate function))
  (when (listp array) (setq array (vconcat array)))
  (my-merge-sort--internal 0 (1- (length array)) array (or predicate #'<)))

;; Packages
(defun my-package-elpa-p (package)
  "Check if PACKAGE is in GNU Elpa."
  (seq-find (lambda (desc)
	      (string= "gnu" (package-desc-archive desc)))
	    (alist-get package package-archive-contents)))

(defun my-package-installed-from-elpa (package)
  "Check if PACKAGE was installed from GNU Elpa.
If not installed or built-in return nil."
  (when (and (package-installed-p package)
	     (not (package-built-in-p package)))
    (< (car (package-desc-version (cadr (assq package package-alist))))
       20000000)))

(defun my-package-available-to-switch ()
  "Return list of packages installed from MELPA that are available on GNU Elpa."
  (seq-remove #'my-package-installed-from-elpa
	      (seq-filter #'my-package-elpa-p package-activated-list)))

;; Thread-safe vector
(cl-defstruct (nov--svector (:copier nil))
  (data (vector))
  (mutex (make-mutex) :read-only t))

(defun nov--svector (&rest objects)
  (make-nov--svector :data (apply #'vector objects)))

(defun nov--svector-ref (svector index)
  (cl-check-type svector nov--svector)
  (with-mutex (nov--svector-mutex svector)
    (aref (nov--svector-data svector) index)))

(defun nov--svector-set (svector index object)
  (cl-check-type svector nov--svector)
  (with-mutex (nov--svector-mutex svector)
    (aset (nov--svector-data svector) index object)))

(defun nov--image-p (path)
  "Return t if PATH refers to an image file."
  ;; HACK: this should be looked up in the manifest
  (--find (string-match-p (car it) path)
	  image-type-file-name-regexps))

(defun nov--html-p (path)
  "Return t if PATH refers to an HTML file."
  nil)

;; This function should replace nov-render-document
(defun nov-show-document ()
  "Show the rendered document referenced by `nov-documents-index'."
  (interactive)
  (with-mutex (nov--svector-mutex nov-documents)
    (let* ((document (nov--svector-ref nov-documents nov-documents-index))
	   (id (car document))
	   (path (cadr document))
	   (doc-str (caddr document))
	   ;; NOTE: allows resolving image references correctly
	   (default-directory (file-name-directory path))
	   buffer-read-only)
      (erase-buffer)
      (cond
       (doc-str
	(insert doc-str))
       ((nov--image-p path)
	(nov-insert-image path ""))
       ((nov--html-p path)
	(insert (nov--cache-document nov-documents-index path)))
       (t
	(insert (nov-slurp path))
	(funcall nov-render-html-function)))
      (goto-char (point-min)))))

;; TODO: Cache only stuff that isn't an image.
(defun nov--cache-document (index path)
  "Render the file referenced by INDEX and cache it in `nov-documents'.
Return the rendered document as a string."
  (with-temp-buffer
    (nov-slurp path)
    (funcall nov-render-html-function)
    (nov--svector-set nov-documents index (buffer-string))))


;; NEW WAY
(defun nov--get-rendered-string (index)
  "Render if necessary"
  (with-mutex (nov--svector-mutex nov-documents)
    (let* ((entry (nov--svector-ref nov-documents index))
	   (id (car entry))
	   (path (cadr entry))
	   (buffer-string (caddr entry)))
      (unless buffer-string
	(setq buffer-string (nov--render-document path))
	(nov--svector-set index (list id path buffer-string)))
      buffer-string)))

;; This successfully renders the document and returns a valid buffer string
;; but that doesn't mean it's done. nov-mode has to be on to render it
;; and the buffer header has to be reset, as well as nov-documents-index.
;; Maybe to not lose much information and do some stuff twice the whole buffer
;; should be named and cached. (generate-new-buffer) should be used for that.
;; This will also have adverse effects - mainly that local variables will have
;; to be copied and the buffers will have to be destroyed after closing the
;; ebook, which is non-trivial. Maybe it's not a way to do it but doing the
;; other thing will result in refactoring some code in nov-render-html.
;; (let ((buffer-read-only nil))
;;   (erase-buffer)
;;   (insert (nov--render-document
;; 	   'id928
;; 	   "/tmp/nov-El9JuV.epub/text/part0003_split_003.html"
;; 	   "2.0")))
(defun nov--render-document (id path epub-version)
  "Return the buffer string of a rendered document"
  (let ((imagep (nov--image-p path))
	(default-directory (file-name-directory path)))
    (with-temp-buffer
      (cond
       (imagep
	(nov-insert-image path ""))
       ((and (version< epub-version "3.0")
	     (eq id nov-toc-id))
	(insert (nov-ncx-to-html path)))
       (t
	(insert (nov-slurp path))))
      (when (not imagep)
	(funcall nov-render-html-function))
      (buffer-string))))

;; The synchronization must be improved. One thread will render all documents
;; one after another, but when the main thread wants to get the cached one
;; it will have to wait for the other to unlock the mutex.

;; NOTE: nov-render-document is bound to 'g', with this it won't re-render the
;; document. There should be a new function to force it.

;; NOTE: The
;; TODO: The rendering part is done. Now to creating a thread that will cache
;;       everything when the ebook is opened, starting with current document.
;; TODO: Find out how to change the page header so it can be replaced when
;;       loading a cached document into a buffer. Also find out which of the
;;       buffer-local variables need to change for it to not cause any
;;       glitches.

(let ((url-format
       (concat "https://exchangerate.guru/system/exchange-rate-chart/"
	       "?amount=40&bcc=%s&scc=%s&dateFrom=%s&dateTo=%s")))
  (defun my-get-currency-rates (from to date-from date-to)
    (with-current-buffer
	(url-retrieve-synchronously
	 (format url-format from to date-from date-to))
      (goto-char url-http-end-of-headers)
      (json-parse-buffer))))

(defun my-get-pln-price (days)
  (let* ((time (current-time))
	 (start-time (time-subtract time (* 86400 30))))
    (my-get-currency-rates "XAU" "PLN"
			   (format-time-string "%Y-%m-%d" start-time)
			   (format-time-string "%Y-%m-%d" time))))
