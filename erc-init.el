;;; erc-init.el --- Initialization file for erc -*- lexical-binding: t; -*-

(require 'subr-x)

(defun add-erc-module (module)
  (unless (member module erc-modules)
    (setopt erc-modules (cons module erc-modules))))

(use-package erc-hl-nicks
  :config (require 'erc-hl-nicks)
  ;; :hook (erc-mode . erc-hl-nicks-mode)
  )

;; ident
(setq
 erc-prompt-for-password nil
 erc-prompt-for-nickserv-password nil)
(require 'erc-track)
(add-hook 'erc-mode-hook 'erc-track-mode)

(defvar my-erc-server-info nil
  "Info on how to connect and identify on a specific server.

This has a format of '((server-name
                        :server server-host
                        :nick user-name
                        :channels (\"channel1\" \"channel2\" ...))
                        :before-functions (my-function-name my-function-2)
                       (server-2-name
                        ...))

server-name - server name as in `erc-server-alist' (case-sensitive)
:server - i.e. \"irc.freenode.net\"
:nick - nick to use on the server
Optional:
:port - port to use for connection, if not specified - use 6697
:channels - list of channels to join after ident
:no-tls - if not nil, use `erc' instead of `erc-tls'
:before-functions - a list of functions to run before connecting to a server

To get specific property from the list, use `my-erc-server-get'")

(defun my-erc-server-get (server prop)
  "Get the PROP of SERVER name from `my-erc-server-info' list."
  (plist-get (cdr (assoc server my-erc-server-info)) prop))

(defun my-erc-maybe-run-Bitlbee ()
  (unless (eq 0 (call-process "pidof" nil nil nil "bitlbee"))
    (call-process "sh" nil t nil
		  (concat (getenv "HOME")
			  "/.config/bitlbee/run-bitlbee.sh"))))

(setq my-erc-server-info
      '(("Rizon"
	 :server "irc.rizon.net"
	 :nick "Oxon"
	 :channels ("#krasnale"))
	("freenode"
	 :server "irc.freenode.net"
	 :nick "lampilelo"
	 :channels ("#emacs" "#emacs-beginners" "#emacs-pl" "#guile"))
	("Libera.Chat"
	 :server "irc.libera.chat"
	 :nick "lampilelo"
	 :channels ("#emacs" "#guile"
		    ;; "#emacs-beginners" "#scheme"
		    ;; "#C++" "#crypto"
		    ))
	("BitlBee"
	 :server "localhost"
	 :nick "Oxon"
	 :port 6667
	 :no-tls t
	 :before-functions (my-erc-maybe-run-Bitlbee))))

(eval-after-load 'erc-services
  '(add-to-list
    'erc-nickserv-alist
    '(Libera.Chat
      "NickServ!NickServ@services.libera.chat"
      "This\\s-nickname\\s-is\\s-registered.\\s-Please\\s-choose"
      "NickServ"
      "IDENTIFY"
      nil
      nil
      "You\\s-are\\s-now\\s-identified\\s-for\\s-")))

(defvar my-erc-password-store-names nil
  "Alist of password names corresponding to entries from `my-erc-server-info'.

Names are stored in cdr of an entry and are supposed to be used with \"pass\"
ulitily on Linux: \"pass password-name\".

Example:
  ((\"Rizon\" . \"rizon/My_nick\")
   (\"freenode\" . \"freenode/My_nick\"))")

(setq my-erc-password-store-names
      '(("Rizon" . "irc.rizon.net/Oxon")
	("freenode" . "irc.freenode.net/lampilelo")
	("Libera.Chat" . "irc.libera.chat/lampilelo")
	("BitlBee" . "bitlbee/Oxon")))

;; Add passwords to erc-nickserv-passwords
;; Gets password from command "pass server/nick"
;;   where server is :server and nick is :nick from `my-erc-server-info'
(with-check-for-missing-packages ("pass") "my-erc-passwords" nil
  (defun my-erc-refresh-passwords ()
    "Reload passwords using \"pass\" command and `my-erc-server-info'."
    (interactive)
    (setq erc-nickserv-passwords nil)
    (let ((ret t))
      (dolist (server-info my-erc-server-info ret)
	(condition-case pass-err
	    (let ((plist (cdr server-info)))
	      (push
	       `(,(intern (car server-info)) ; server name as symbol
		 ((,(plist-get plist :nick) .
		   ,(with-temp-buffer
		      (if (eq 0 (call-process
				 "/usr/bin/pass" nil (current-buffer) nil
				 ;; get arg from my-erc-password-store-names
				 (or (assoc-default
				      (car server-info)
				      my-erc-password-store-names)
				     (error (format
    "Couldn't retrieve %s profile password from `my-erc-password-store-names'"
					     (car server-info))))))
			  (string-trim-right (buffer-string))
			(error (format "No password for %s"
				       (car server-info))))))))
	       erc-nickserv-passwords))
	  (error (display-warning "erc-init.el"
				  (error-message-string pass-err))
		 (setq ret nil)))))
    "Loading passwords finished."))

(defmacro my-erc--define-connect-function (server-name)
  "Create an interactive function for connecting to a specific server.
E.g. \"irc-freenode\".

Uses `my-erc-server-info' to get the information about server settings."
  (when (assoc server-name my-erc-server-info)
    (let ((fun-name (intern (concat "irc-" server-name))))
      `(defun ,fun-name ()
	 ,(concat "Run erc on " server-name " server.

Uses `my-erc-server-info' to get the information about server settings.")
	 (interactive)
	 ,(when (fboundp #'my-erc-refresh-passwords)
	    `(unless (assoc ',(intern server-name) erc-nickserv-passwords)
	       (if (assoc ,server-name my-erc-password-store-names)
		   (my-erc-refresh-passwords)
		 (message "Warning: no password found for server %s"
			  ,server-name))))
	 (mapc #'funcall (my-erc-server-get ,server-name :before-functions))
	 (,(if (my-erc-server-get server-name :no-tls) 'erc 'erc-tls)
	  :server (my-erc-server-get ,server-name :server)
	  :port (or (my-erc-server-get ,server-name :port) 6697)
	  :nick (my-erc-server-get ,server-name :nick))))))

(dolist (serv '("Rizon" "Libera.Chat" "BitlBee"))
  (eval `(my-erc--define-connect-function ,serv)))

(setq erc-fill-column 76)

;; TODO: This should be probably removed since I use custom code for ident
(require 'erc-services)
(add-hook 'erc-mode-hook 'erc-nickserv-mode)

;; logs
(require 'erc-log)
(add-hook 'erc-mode-hook 'erc-log-mode)
(add-hook 'erc-mode-hook 'erc-log-enable)
(setq erc-log-channels-directory "~/.emacs.d/erc/logs/")
(unless (and (file-exists-p erc-log-channels-directory)
	     (file-directory-p erc-log-channels-directory))
  (mkdir erc-log-channels-directory t))
(setq erc-save-buffer-on-part t
      erc-save-queries-on-quit t)
;; (setq erc-log-insert-log-on-open t)
;; load modules
;; (require 'erc-services)
(add-hook 'erc-mode-hook #'(lambda ()
			     (erc-services-mode 1)
			     (add-erc-module 'notifications)
			     (erc-services-enable)))
;; (setq erc-notifications-enable t)
;; (erc-services-enable)
;; (setq erc-services-enable 1)

(setq erc-notifications-icon
      "/usr/share/icons/hicolor/128x128/apps/emacs.png")

;; auto-join channels
(add-erc-module 'autojoin)
(setopt erc-autojoin-channels-alist
	(mapcar (lambda (item)
		  (cons (plist-get (cdr item) :server)
			(plist-get (cdr item) :channels)))
		my-erc-server-info))

(setq erc-autojoin-timing 'ident)
;; end of auto-join

;; erc-match
(add-erc-module 'match)
;; (require 'erc-match)
(setopt erc-fool-highlight-type 'message)

(load (concat user-emacs-directory "erc-match-lists.el") t)
;;

;; NOTE: I removed this because erc-server-NOTICE already does what I want,
;;       apparently. With this on, all notice messages would be printed twice
;;       in the server buffer.
;; (add-hook 'erc-echo-notice-always-hook 'erc-echo-notice-in-server-buffer)

;; Print all NOTICE functions in the server buffer and nowhere else.
;; There is one problem with this approach. With erc-track excluding server
;; buffers there's no notification for NOTICE anywhere.
(defun my-erc-server-notice-handler (proc parsed)
  (erc-server-NOTICE proc parsed)
  ;; (erc-display-message parsed nil 'active (erc-response.contents parsed))
  nil)
(add-hook 'erc-server-NOTICE-functions 'my-erc-server-notice-handler)

;; FIXME:
;; This is a workaround for erc-track disabled for server buffers.
;; What I would ultimately like to do is to show notice messages only in the
;; server buffer and track only them. Probably also show notices from users
;; in the current active buffer as well.
(add-hook 'erc-echo-notice-always-hook 'erc-echo-notice-in-active-buffer)

;; FIXME: Notice messages still print twice.

;; misc options
;; Don't switch to buffers on join
(setopt erc-join-buffer 'bury)
;; Kill buffers for channels after /part
(setopt erc-kill-buffer-on-part t)
;; Kill buffers for private queries after quitting the server
(setopt erc-kill-queries-on-quit t)
;; Kill buffers for server messages after quitting the server
(setopt erc-kill-server-buffer-on-quit t)
;; Interpret mIRC-style color commands in IRC chats
(setopt erc-interpret-mirc-color t)
;; Rename server buffers to only the server name part
(setopt erc-rename-buffers t)
;; Ignore certain type of messages when showing on the modeline
;; 324 - Channel or nick modes
;; 329 - Channel creation date notice
;; 353 - NAMES notice (ignored by default)
(dolist (item '("JOIN" "PART" "QUIT" "MODE" "TOPIC" "NICK" "324" "329"))
  (add-to-list 'erc-track-exclude-types item))
(setopt erc-track-exclude-server-buffer t)
(push "&bitlbee" erc-track-exclude)

;; Workaround for a bug. It made erc reuse a buffer when trying to connect
;; to the same name channel on a different network.
;; (setq erc-reuse-buffers nil)

;; sound notifications
(defun my-erc-play-notification-sound ()
  "Play the freedesktop message-new-instant sound."
  (start-process
   "erc-notification-sound" nil
   "ffplay" "-vn" "-nodisp" "-t" "1" "-autoexit"
   "/usr/share/sounds/freedesktop/stereo/message-new-instant.oga"))

(defun my-erc-system-notification (msg)
  (start-process
   "erc-system-notification" nil
   "notify-send"
   "--icon=/usr/share/icons/hicolor/128x128/apps/emacs.png"
   "ERC"
   msg))

(defun erc-my-privmsg-sound (proc parsed)
    (let* ((tgt (car (erc-response.command-args parsed)))
           (privp (erc-current-nick-p tgt)))
      (when privp
	(my-erc-play-notification-sound)
	(x-urgent)))
    nil)
(add-hook 'erc-server-PRIVMSG-functions 'erc-my-privmsg-sound)

(defun my-erc-play-sound-on-my-nick ()
  (goto-char (point-min))
  (when (search-forward (erc-current-nick) nil t)
    (my-erc-play-notification-sound))
  nil)
(add-hook 'erc-insert-post-hook 'my-erc-play-sound-on-my-nick)

;; Show message whenever ctcp request is issued.
(defun my-erc-ctcp-notice (proc parsed)
  "Show CTCP request messages in the active buffer and the server buffer."
  (let ((msg (erc-response.contents parsed)))
    ;; if message is CTCP
    (when (erc-is-message-ctcp-and-not-action-p msg)
      (dolist (where `(,nil active))
	(erc-display-line
	 (format "-CTCP- %s request from %s"
		 ;; (format "%s" parsed)
		 (replace-regexp-in-string "" "" msg)
		 (erc-response.sender parsed))
	 where))))
  nil)
(add-hook 'erc-server-PRIVMSG-functions 'my-erc-ctcp-notice)

;; (my-erc-system-notification "&lt;nick&gt; message")

(with-eval-after-load 'erc-notify
 (defun my-erc-notice-signon-notify (server nick)
   (my-erc-system-notification (format "Detected %s on IRC network %s"
				       nick server))
   (my-erc-play-notification-sound))

 (add-hook 'erc-notify-signon-hook #'my-erc-notice-signon-notify))


(with-eval-after-load 'erc-list
  (defun my-erc-list-sort ()
    (interactive)
    (if (eq major-mode 'erc-list-menu-mode)
	(let ((inhibit-read-only t))
	  (sort-numeric-fields 2 (point-min) (point-max))
	  (reverse-region (point-min) (point-max)))
      (user-error "Not an erc list buffer")))

  (define-key erc-list-menu-mode-map (kbd "s") #'my-erc-list-sort))

(defun my-erc-goto-log-file ()
  (interactive)
  (when (eq major-mode 'erc-mode)
    (find-file (erc-current-logfile (current-buffer)))))

;; FIXME: all of this can be done by
;;        (setq erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
;;        (setq erc-lurker-threshold-time 3600)
;;        Check if nick change recognition is as good as in my version, if not
;;        maybe make a pull request?
;; Hide join, quit, part and nick messages of users that haven't spoken in the
;; current session
;; TODO: Create a hash table for every server. (For now there's one, global)
;;       Maybe buffer-local for every server buffer?

;; 1. Create a hash-table of users that have spoken in the current session
;; It's local for every channel.
;; TODO: setq this variable when opening a new channel
(defvar my-erc-relevant-users (make-hash-table :test #'equal)
  "List of ERC users that have spoken in the current session.
JOIN, PART, QUIT and NICK messages about them will be shown in the
channel buffer.
Messages about other users will be ignored.")
(make-variable-buffer-local 'my-erc-relevant-users)

(defvar my-erc-relevance-timeout 3600
  "Time in seconds since user's last activity after which he becomes
irrelevant. JOIN, PART, QUIT and NICK messages related to him will not be
shown.")

;; 2. Add a hook to user messages (whatever it is) to populate this table
(defun my-erc-add-relevant-user (message)
  (save-match-data
    (cond
     ;; user messages (<nick> message text)
     ;; this will update user's last activity time
     ((string-match
       (rx (seq line-start "<"
		(group (one-or-more (not (any blank ?\>)))) ">"))
       message)
      (puthash (match-string-no-properties 1 message)
	       (current-time)
	       my-erc-relevant-users))
     ;; nick change (*** nick (...) is now known as new_nick)
     ;; this will copy old nick's last activity data to the new one
     ((string-match
       (rx (seq line-start "*** " (group (+ (not whitespace))) (* not-newline)
		"is now known as " (group (+ (not whitespace))) line-end))
       message)
      (let ((last-activity
	     (gethash (match-string-no-properties 1 message)
		      my-erc-relevant-users)))
	(when last-activity
	  (puthash (match-string-no-properties 2 message)
		   last-activity
		   my-erc-relevant-users)))))))
(add-hook 'erc-insert-pre-hook #'my-erc-add-relevant-user)

;; 3. Capture join, part, quit and nick messages and show only those related
;;    to relevant users from the hash table
(defun my-erc-filter-irrelevant-messages (message)
  (save-match-data
    (when (string-match
	   (rx (seq line-start "*** " (group (+ (not whitespace)))
		    (* not-newline) (or "has joined channel"
					"has left channel" "has quit"
					"is now known as")))
	   message)
      (let ((last-activity (gethash (match-string-no-properties 1 message)
				    my-erc-relevant-users)))
	(unless (and last-activity
		     (< (time-to-seconds
			 (time-subtract (current-time) last-activity))
			my-erc-relevance-timeout))
	  (setq erc-insert-this nil))))))
(add-hook 'erc-insert-pre-hook #'my-erc-filter-irrelevant-messages t)

;; =============================================

(defun my-erc-quit-all-servers ()
  "Quit all open ERC sessions"
 (mapc (lambda (buffer)
	 (when (erc-server-buffer-p buffer)
	   (with-current-buffer buffer
	     (erc-cmd-QUIT (erc-quit-reason-normal)))))
       (buffer-list))
 nil)

(add-hook 'kill-emacs-hook #'my-erc-quit-all-servers)


;; ZNC
(with-eval-after-load 'znc
  (setq
   znc-servers
   `(("lelserv" 4660 t
      ((libera
	"znc"
	,(string-trim (shell-command-to-string "/usr/bin/pass znc/znc"))))))))


(defun irc-znc ()
  (interactive)
  (let ((gnutls-verify-error nil))
    ;; NOTE: This is dumb and probably won't even work.
    (erc-nickserv-identify-mode nil)
    (znc-erc "libera")))

(defun irc-Libera.Chat ()
  (interactive)
  (let ((erc-sasl-mechanism 'plain)
	(erc-sasl-user "lampilelo")
	(erc-sasl-auth-source-function
	 (lambda (&rest _)
	   (string-trim
	    (shell-command-to-string
	     "/usr/bin/pass irc.libera.chat/lampilelo")))))
    (erc-tls :server "irc.libera.chat" :port 6697 :nick "lampilelo")))

(add-erc-module 'sasl)
